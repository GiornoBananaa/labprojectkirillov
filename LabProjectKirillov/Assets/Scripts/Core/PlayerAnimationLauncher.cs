using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationLauncher : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    public void PlayWalk(bool loob)
    {
        _animator.SetBool("Walk", loob);
    }
    public void PlayJump()
    {
        _animator.SetTrigger("Jump");
    }
    public void PlayUnjump()
    {
        _animator.SetTrigger("Unjump");
    }
}
