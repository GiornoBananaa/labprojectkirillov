using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    [SerializeField] private int _speed;
    [SerializeField] private int _jumpForce;
    [SerializeField] private TMP_Text _coinText;
    [SerializeField] private GameObject _coinPrefab;
    [SerializeField] private Camera _camera;
    [SerializeField] private PlayerAnimationLauncher _animLauncher;

    private Rigidbody _rigidbody;
    private int _coins;
    private bool _isMoving;
    private bool _isJumping;

    void Start()
    {
        _isMoving = false;
        _rigidbody = GetComponent<Rigidbody>();
        _coins = 0;
    }
    void Update()
    {
        Debug.Log(_rigidbody.velocity.x + " | " + _rigidbody.velocity.y + " | " + _rigidbody.velocity.z);
        if (!_isMoving & new Vector3(_rigidbody.velocity.x, 0, _rigidbody.velocity.z).magnitude > 0.2)
        {
            _animLauncher.PlayWalk(true);
            _isMoving = true;
        }
        else if (_isMoving & new Vector3(_rigidbody.velocity.x, 0, _rigidbody.velocity.z).magnitude < 0.2)
        {
            _animLauncher.PlayWalk(false);
            _isMoving = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && new Vector3(0, _rigidbody.velocity.y, 0).magnitude < 0.1)
        {
            _animLauncher.PlayJump();
            _rigidbody.AddForce(Vector3.up * _jumpForce);
            _isJumping = true;
        }
    }
    void FixedUpdate()
    {

        float x = 8 * Input.GetAxis("Mouse X");
        transform.Rotate(Vector3.up * x);

        if (Input.GetKey(KeyCode.W))
        {
            _rigidbody.AddForce(transform.forward * _speed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.AddForce(-transform.forward * _speed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.AddForce(transform.right * _speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.AddForce(-transform.right * _speed);
        }

        if (!Input.GetKey(KeyCode.A) & !Input.GetKey(KeyCode.D) & !Input.GetKey(KeyCode.S) & !Input.GetKey(KeyCode.W))
        {

            if (_rigidbody.velocity.x != 0)
            {
                _rigidbody.velocity = new Vector3(Mathf.Lerp(_rigidbody.velocity.x, 0, 0.1f), _rigidbody.velocity.y, _rigidbody.velocity.z);
            }
            if (_rigidbody.velocity.z != 0)
            {
                _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, _rigidbody.velocity.y , Mathf.Lerp(_rigidbody.velocity.z, 0, 0.1f));
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Coin")
        {
            Destroy(other.gameObject);
            CoinEarned();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            if (_isJumping)
            {
                _animLauncher.PlayUnjump();
                _isJumping = false;
            }
        }
    }

    void CoinEarned()
    {
        _coins++;
        _coinText.text = $"Coins: <b>{_coins}";
        Instantiate(_coinPrefab, new Vector3(Random.Range(-15,15), 1, Random.Range(-15, 15)), _coinPrefab.transform.rotation);
    }
}
